# Radiation detector control system
# Can be used with the Radiation_v1, radiation detection
# software written for Arduino by Daan Roos
#
# Uses serial to communicate with the Arduino, record data,
# and control the threshold voltage
#
# The program uses the following packages: PySimpleGUI, numpy, matplotlib,
# time, multiprocessing, serial, and datetime
#
# Author: Daan Roos

# mainM definition
## Function that prints messages sent by the main process
def mainM(message):
    if __name__ == '__main__':
        print("Process 1 - " + message)

# Libraries
mainM("Loading libraries")
import PySimpleGUI as sg
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import time
from multiprocessing import Process
from multiprocessing import Manager
import serial
import datetime

# Creating process-spanning variables in the main process
mainM("Loading data")
if __name__ == '__main__':
    manager = Manager()
    x_data = np.linspace(0,500,500)
    y_data = np.linspace(0,0,500)
    shared_x = manager.list(x_data.tolist())
    shared_y = manager.list(y_data.tolist())
    shared_commands = manager.list()
    threshold = manager.list([5.0])
    ticks = manager.list([0,-1])
    trigger = manager.list([0])

# Creating local variables
comPort = "COM16"
currentTicks = 0
currentThresh = -1

# Dictionary for text shown in textbox in the bottom of the screen
log = {
    "text": [],
    "updated": True
}

# addLog definition
## Function that adds a new log to the textbox, including the time of the new line
def addLog(line):
    logtime = datetime.datetime.now()
    timestring = str(logtime.hour) + ":" + str(logtime.minute) + ":" + str(logtime.second) + " - "
    log["text"].append(timestring + line)
    log["updated"] = True
    # Update the textbox containing the log
    window.FindElement('logger').Update("\n".join(log["text"]))


# Layout of window
# --------------------------------
# | | on |Ticks|Trigger|Thresh | |
# | | off|     |       | Info  | |
# | ---------------------------- |
# | |                          | |
# | |          Plot            | |
# | |                          | |
# | ---------------------------- |
# | |           log            | |
# | ---------------------------- |
# | Software info                |
# --------------------------------

# The following array will be used by PySimpleGUI to create the layout
layout = [
    [
        # Collumn containing connection to Arduino, and the start/stop button for recording data
        sg.Column(
            [
                [
                    sg.InputText(size=(20,1)),
                    sg.B('Connect')
                ],
                [
                    sg.B('Start/Stop')
                ]
            ]
        ),
        # Collumn containing recorded peaks, peaks from arduino will be Unknown by default
        sg.Column(
            [
                [
                    sg.T("Current Ticks: "),
                    sg.T("0000000", key="Ticks")
                ],
                [
                    sg.T("Arduino Ticks: "),
                    sg.T("Unknown", key="ATicks")
                ]
            ]
        ),
        # Checkbox for enabling trigger
        sg.Checkbox('Enable trigger'),
        # Collumn for setting threshold and pulling arduino info (peaks, threshold)
        sg.Column(
            [
                [
                    sg.InputText(size=(20,1), key="thresh"),
                    sg.Column(
                        [
                            [sg.B('Set threshold')],
                            [sg.B("Get Arduino info!")]
                        ]
                    )
                ]
            ]
        )
    ],    
    # Collumn for plot
    [sg.Column(
        layout=[
            [sg.Canvas(key='fig_cv',
                       size=(400 * 2, 400)
                       )]
        ],
        background_color='#DAE0E6',
        pad=(0, 0)
    )],
    # Textbox for logging
    [sg.Multiline(
                    key='logger',
                    font=('Courier New', 9),
                    autoscroll=True,
                    size=(115, 10),
                    pad=(0, (15, 0)),
                    background_color='white',
                    disabled=True)],
    [
        sg.T("Made for: Radiation_v1, radiation detection on Arduino | Daan Roos, 2021")
    ]
]

mainM("Loading main definitions")
# Puts figure inside of window
def drawFig(canvas, fig):
    if canvas.children:
        for child in canvas.winfo_children():
            child.destroy()
    figure_canvas_agg = FigureCanvasTkAgg(fig, master=canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='right', fill='both', expand=1)
    return figure_canvas_agg

# When main process, will create the window and the figures
if __name__ == '__main__':
    window = sg.Window('Radiation detector control system | Daan Roos', layout, finalize=True)
    addLog("Starting!")
    # Initializes figure inside window
    fig = plt.figure(1)
    DPI = fig.get_dpi()
    fig.set_size_inches(404*2/float(DPI), 404/float(DPI))
    graph = plt.plot(x_data,y_data)[0]
    plt.title('Arduino data')
    plt.xlabel('Samples')
    plt.ylabel('Voltage')
    plt.grid()
    plt.ylim([0,0.05])
    can = drawFig(window['fig_cv'].TKCanvas, fig)
    plt.close(1)
    addLog("Prepared figure!")

# Functions for second thread for handling incoming data
mainM("Loading second thread definitions")
checking_tick = False

# procM definition
## Prints messages sent by second process
def procM(message):
    print("Process 2 - " + message)

# push_back definition
## Adds new data to the back of array printed on plot
## Oldest data will be removed
## Also handles peak detection
def push_back(y,shared_x,shared_y,ticks,trigger):
    global checking_tick
    # When the trigger is set, it will halt when
    # peak is in center of figure
    if(trigger[0] == 1 and shared_x[249] == 1):
        return 0
    # Pushes oldest data away and new data at the end of array
    shared_x[0:len(shared_x)-1] = shared_x[1:len(shared_x)]
    shared_x[len(shared_x)-1] = 0
    shared_y[0:len(shared_y)-1] = shared_y[1:len(shared_y)]
    shared_y[len(shared_y)-1] = y
    # When a peak is detected
    if(y > 0 and not(checking_tick)):
        # Adds 1 too amount of peaks
        ticks[0] = ticks[0] + 1
        # Sets position on x-axis to 1 of peak
        # This is used by trigger mechanism
        shared_x[len(shared_x)-1] = 1
        checking_tick = True
    # When the detected peak is gone
    elif(y == 0 and checking_tick):
        checking_tick = False

# update_threshold definition
## Updates the current threshold to the new threshold 
## received from Arduino
def update_threshold(newthreshold,shared_x,shared_y,threshold):
    threshold[0] = float(newthreshold)

# update_arduinoCount
## Updates the peak count of the Arduino
def update_arduinoCount(count,shared_x,shared_y,ticks):
    print(count)
    ticks[1] = count

# readSerial definition
## Reads a 12 character string from the serial connection
## converts it to ascii
def readSerial(ser):
    byte_buffer = ser.read(12)
    return byte_buffer.decode('ascii')

# parseSerial definition
## Parses the received data depending on the first character in 
## the string:
## D: data recorded of the detector
## T: the threshold set on the Arduino
## C: the peaks counted by the Arduino
def parseSerial(id,threshold,data,shared_x,shared_y,ticks,trigger):
    if(id == 'D'):
        push_back(float(data),shared_x,shared_y,ticks,trigger)
        return
    elif(id == 'T'):
        update_threshold(float(data),shared_x,shared_y,threshold)
        return
    elif(id == 'C'):
        update_arduinoCount(int(data),shared_x,shared_y,ticks)
        return

# handleCommands definition
## Checks for commands requested by main process
def handleCommands(ser,commands,shared_x,shared_y,threshold):
    if(len(commands) == 0):
        return 0
    for command in commands:
        # Command for the start/stop button
        if(command == 'condiscon'):
            # sends L0000000 to indicate the toggle of recording data
            ser.write(b"L0000000")
            procM("Start/stop sent")
            # Flush all serial buffers
            ser.flush()
            ser.flushInput()
            ser.flushOutput()
        # Command for requesting the data stored on Arduino
        elif(command == 'info'):
            ser.write(b"I0000000")
            procM("Info request sent")
        # Command for setting the threshold value on the arduino
        elif(command == 'thresh'):
            # Creates command string using selected threshold voltage
            com = "T%.5f" % threshold[0]
            ser.write(bytes(com,'ascii'))
            procM("Threshold sent")
    commands[:] = []

# handle_data definition
## Opens the serial port, makes sure requested commands and incoming data is parsed
def handle_data(port,threshold,shared_x,shared_y,shared_commands,ticks,trigger):
    ser = serial.Serial(timeout=1)
    ser.port = port
    ser.open()
    ser.baudrate = 19200
    # Sleep 4 seconds to ensure a good connection (Arduino has to reboot)
    time.sleep(4)
    
    while True:
        # If there is any data on the serial bus
        if(ser.in_waiting > 0):
            # Read 12 characters
            data_buffer = readSerial(ser)
            if(len(data_buffer) < 12):
                continue
            parseSerial(data_buffer[0],threshold,data_buffer[1:len(data_buffer)-1],shared_x,shared_y,ticks,trigger)
        handleCommands(ser,shared_commands,shared_x,shared_y,threshold)
    
p = False

mainM("Starting program")
# spawnProcess definition
## Creates a new process for handling serial communication
def spawnProcess():
    global p
    # Reset all process-wide variables
    shared_y[:] = np.zeros(500).tolist()
    shared_x[:] = np.zeros(500).tolist()
    ticks[0] = 0
    ticks[1] = -1
    # When p is set, a process is already running
    if(p):
        addLog("Killing previous serial connection!")
        p.terminate()
    addLog("Starting new serial connection!")
    
    # Create a new process that will run the handle_data function
    p = Process(target=handle_data,args=(comPort,threshold,shared_x,shared_y,shared_commands,ticks,trigger))
    p.start()
    # Wait 5 seconds to ensure the process is booted correctly
    time.sleep(5)
    addLog("Started serial connection!")

paused = True

# When the process is the main process, 
# the events of the window will be managed here
if __name__ == '__main__':
    # Event loop
    while True:
        # Read for events every 10 ms
        event, values = window.read(timeout=10)
        # Closing of window
        if event in (sg.WIN_CLOSED, 'Exit'):
            break
        # Pressing the connect button
        elif event == 'Connect':
            if values[0] != "":
                comPort = values[0]
                spawnProcess()
        # Pressing the start/stop button
        elif event == 'Start/Stop':
            addLog("Sending start/stop command!")
            # Request the command for start/stop
            shared_commands.append('condiscon')
            # Reset all process-wide variables
            shared_y[:] = np.zeros(500).tolist()
            shared_x[:] = np.zeros(500).tolist()
            ticks[0] = 0
            ticks[1] = -1
            trigger[0] = 0
            # Switch whether real-time plotting should be paused
            paused = not(paused)
        # Pressing the Set threshold button
        elif event == 'Set threshold':
            try:
                val = float(values["thresh"])
            except:
                addLog("ERROR: Define a number between 0 and 5V!")
                continue
            threshold[0] = val
            # Request command for setting the threshold
            shared_commands.append('thresh')
            addLog("Setting threshold of " + values["thresh"] + "V")
        # Pressing the Get Arduino info button
        elif event == "Get Arduino info!":
            # Request command for pulling info stored onarduino
            shared_commands.append('info')
            addLog("Requesting info!")
        # default statement for rendering the figure and
        # handling other data
        elif not(paused):
            # values[1] indicates whether the trigger checkbox is checked
            # Trigger is disabled
            if(values[1] == False):
                trigger[0] = 0
                graph.set_ydata(shared_y)
            # Trigger is enabled and peak is at center of figure
            elif(shared_x[249] == 1):
                graph.set_ydata(shared_y)
                addLog("Triggered! Sending start/stop command!")
                # Peak is at center, so pause real-time plotting
                paused = True
                # Request the start/stop command
                shared_commands.append('condiscon')
                # Draw final changes
                can.draw()
                plt.pause(0.01)
                # Wait for Arduino to respond
                time.sleep(2)
                # Skip this event cycle to ensure data in figure does not change
                continue
            # Trigger is enabled but peak is not at center of figure
            else:
                trigger[0] = 1
                graph.set_ydata(np.zeros(500))
            # Draw the created updates to the figure
            can.draw()
            plt.pause(0.01)
            # When the amount of ticks, the threshold or the ticks of 
            # the arduino are updated, change their text to match the new data
            if currentTicks != ticks[0]:
                print(ticks[0])
                print(str(ticks[0]))
                window.Element('Ticks').Update(value=ticks[0])
            if currentThresh != threshold[0]:
                window.Element('thresh').Update(value=str(threshold[0]))
                currentThresh = threshold[0]
            if ticks[1] != -1:
                window.Element('ATicks').Update(value=str(ticks[1]))
                ticks[1] = -1
    # When the close button is pressed, close the window and destroy the seconds process
    window.close()
    p.terminate()
