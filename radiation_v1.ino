// Radiation detection software for Arduino.
// Can be used with the 'Radiation detector control system' by Daan Roos written in Python
//
// Author: Daan Roos

#define COMP_IN A0
#define COMP_THRESH 3

// Setting variables

float threshold_voltage = 5;
int current_count = 0;
bool locked = true;

// setThreshold definition
/// Function that converts floating point to a number between 0-1023
/// and sets the PWM of pin COMP_THRESH
void setThreshold(float new_threshold) {
  threshold_voltage = min(5,new_threshold);
  int raw_threshold = (int) (threshold_voltage/5 * 1023);
  analogWrite(COMP_THRESH,raw_threshold);
}

// sendData definition
/// Function that reads the current voltage level at pin COMP_IN and
/// sends it over serial to the computer
void sendData() {
  // Calculate voltage from a value of 0-1023
  int raw_data = analogRead(COMP_IN);
  float voltage = (float)raw_data/1023 * 5;

  // Translates floating point into string
  char float_string[12];
  dtostrf(voltage,11,9,float_string);

  // Combine string to create a string of following type:
  // Dx.xxxxxxxxx where x is the current voltage
  char data_string[13];
  sprintf(data_string,"D%s", float_string);

  Serial.print(data_string);

  // If the voltage is larger than zero, it will mark it as a 'peak'
  // More precise peak counting will be done on computer side
  if(voltage > 0) current_count++;
}

// parseSerial definition
/// Function that receives a 8 character long array containing data
void parseSerial(char* databuffer) {
  // The first character indicates the type of data:
  // T: a new threshold
  // I: request for information stored on Arduino
  // L: locks or unlocks reading the detection circuit
  switch(databuffer[0]) {
    case 'T':
    {
      // Convert received string to a floating point number
      unsigned int new_threshold_int = 0;
      long new_threshold_dec = 0;
      // Since Arduino does not support floating points in sscanf,
      // the data will be created by combining integers
      sscanf(databuffer,"T%i.%5ld",&new_threshold_int,&new_threshold_dec);

      // Send the new threshold to setThreshold
      setThreshold((float)new_threshold_int + (float)new_threshold_dec/100000);

      // Reset the counted peaks
      current_count = 0;
    }
    break;
    case 'I':
    {
      // Create the information strings
      char infostring[13];

      char number_string[12];
      dtostrf(threshold_voltage,11,9,number_string);
      // Threshold string will be: Txxxxxxxxxxx, where x is the current threshold
      sprintf(infostring,"T%s",number_string);
      Serial.print(infostring);

      // Peak count string will be Cxxxxxxxxxxx, where x is the current peak count
      sprintf(infostring,"C%011i",current_count);
      Serial.print(infostring);
    }
    break;
    case 'L':
    {
      locked = !locked;
    }
    break;
  }
}

void setup() {
  // Prepare all pins
  pinMode(COMP_IN,INPUT);
  // Can be used to control the voltage applied to the threshold pin of the comparator
  pinMode(COMP_THRESH,OUTPUT); 
  analogWrite(COMP_THRESH,255);

  // Setup serial
  Serial.begin(19200);
}

char buffer[9];
int bufcount = 0;

void loop() {
  // If data is available on serial bus, send to parseSerial after
  // 8 characters have been collected
  if(Serial.available() > 0) {
    buffer[bufcount] = Serial.read();
    bufcount++;
    if(bufcount >= 8) {
      buffer[8] = '\0';
      bufcount = 0;
      parseSerial(buffer);
    }
  }

  // When the reading is unlocked, start recording and sending data
  if(!locked) sendData();
}
